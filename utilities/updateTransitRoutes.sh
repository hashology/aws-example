#!/bin/bash

sleep 15 #let vpc api refresh


REGIONS=(us-west-1 us-west-2 us-east-1)
PRIMARY_REGION=us-east-1
PRIMARY_TGW_NAME=core-tgw
PRIMARY_TGW=$(aws --region $PRIMARY_REGION ec2 describe-transit-gateways --filters "Name=tag:Name,Values=$PRIMARY_TGW_NAME" --query 'TransitGateways[*].TransitGatewayId' --output text)
PRIMARY_TGW_RTB=$(aws --region $PRIMARY_REGION ec2 describe-transit-gateways --filters "Name=tag:Name,Values=$PRIMARY_TGW_NAME" --query 'TransitGateways[*].Options.AssociationDefaultRouteTableId' --output text)
PRIMARY_TGW_PEERS=$(aws --region $PRIMARY_REGION ec2  describe-transit-gateway-attachments --filters "Name=transit-gateway-id,Values=$PRIMARY_TGW" "Name=resource-type,Values=peering" --query "TransitGatewayAttachments[*].[ResourceId,TransitGatewayAttachmentId]" --output text | sed 's/[[:space:]]/:/')
PRIMARY_TGW_VPCS=$(aws --region $PRIMARY_REGION ec2  describe-transit-gateway-attachments --filters "Name=transit-gateway-id,Values=$PRIMARY_TGW" "Name=resource-type,Values=vpc" --query "TransitGatewayAttachments[*].ResourceId" --output text)
PRIMARY_TGW_VPCS_SINGELINE=$(echo "$PRIMARY_TGW_VPCS" | tr '\n' ',' | sed 's/,$/\n/')
PRIMARY_ATTACHED_CIDR=$(aws --region $PRIMARY_REGION ec2 describe-vpcs --vpc-ids $PRIMARY_TGW_VPCS_SINGELINE --query "Vpcs[*].CidrBlock" --output text)

echo "Primary TGW: ($PRIMARY_TGW_NAME) : $PRIMARY_TGW in $PRIMARY_REGION"


function propagateRoute () {
    declare -a LEFT_RTBS=()
    declare -a RIGHT_RTBS=()

    LEFT_VPCS=$(aws --region $1 ec2 describe-transit-gateway-attachments --filters "Name=transit-gateway-id,Values=$2" "Name=resource-type,Values=vpc" --query "TransitGatewayAttachments[*].ResourceId" --output text)
    LEFT_VPCS_SINGELINE=$(echo "$LEFT_VPCS" | tr '\n' ',' | sed 's/,$/\n/')
    LEFT_ATTACHED_CIDR=$(aws --region $1 ec2 describe-vpcs --vpc-ids $LEFT_VPCS_SINGELINE --query "Vpcs[*].CidrBlock" --output text)

    for vpc in $LEFT_VPCS; do
        LEFT_RTBS+=($(aws --region $1 ec2 describe-route-tables --filters "Name=vpc-id,Values=$vpc" --query "RouteTables[*].RouteTableId" --output text))
    done

    echo "Updating Left Side VPC RTBs"
    for rtb in ${LEFT_RTBS[*]}; do
        for right_cidr in $6; do
            aws --region $1 ec2 create-route --route-table-id $rtb --destination-cidr-block $right_cidr --transit-gateway-id $2 >/dev/null
        done
    done 
    echo "Updated Left Side VPC RTBs"
    LEFT_TGW_RTB=$(aws --region $1 ec2 describe-transit-gateways --transit-gateway-ids $2 --query "TransitGateways[*].Options.AssociationDefaultRouteTableId" --output text)

    echo "Updating Left Side TGW RTB: $LEFT_TGW_RTB"
    for right_cidr in $6; do
        aws --region $1 ec2 delete-transit-gateway-route --transit-gateway-route-table-id $LEFT_TGW_RTB --transit-gateway-attachment-id $3 --destination-cidr-block $6 >/dev/null 2>&1
        aws --region $1 ec2 create-transit-gateway-route --transit-gateway-route-table-id $LEFT_TGW_RTB --transit-gateway-attachment-id $3 --destination-cidr-block $6 >/dev/null 2>&1
    done
    
    echo "Updated Left Side TGW RTB: $LEFT_TGW_RTB"

    for vpc in $7; do
        RIGHT_RTBS+=($(aws --region $4 ec2 describe-route-tables --filters "Name=vpc-id,Values=$vpc" --query "RouteTables[*].RouteTableId" --output text))
    done

    echo "Updating Right Side VPC RTB"
    for rtb in ${RIGHT_RTBS[*]}; do
        for left_cidr in $LEFT_ATTACHED_CIDR; do
            aws --region $4 ec2 create-route --route-table-id $rtb --destination-cidr-block $left_cidr --transit-gateway-id $5 >/dev/null
        done
    done
    echo "Updated Right Side VPC RTB"

    RIGHT_TGW_RTB=$(aws --region $4 ec2 describe-transit-gateways --transit-gateway-ids $5 --query "TransitGateways[*].Options.AssociationDefaultRouteTableId" --output text)

    echo "Updating Right Side TGW RTB"
    for left_cidr in $LEFT_ATTACHED_CIDR; do
        aws --region $4 ec2 delete-transit-gateway-route --transit-gateway-route-table-id $RIGHT_TGW_RTB --destination-cidr-block $left_cidr >/dev/null 2>&1
        aws --region $4 ec2 create-transit-gateway-route --transit-gateway-route-table-id $RIGHT_TGW_RTB --transit-gateway-attachment-id $3 --destination-cidr-block $left_cidr >/dev/null 2>&1
    done
    echo "Updated Right Side TGW RTB"
}

for i in $PRIMARY_TGW_PEERS; do
    peer=(${i//:/ })
    echo "(Starting) Primary TGW Peer : ${peer[0]} (attachment) ${peer[1]}"
    FOUND_PEER_REGION=""
    for region in ${REGIONS[*]}; do
        aws --region $region ec2 describe-transit-gateways --transit-gateway-ids ${peer[0]} >/dev/null 2>&1
        if [ $? -eq 0 ]
        then
            FOUND_PEER_REGION=$region
        fi
    done
    if [ -z "$FOUND_PEER_REGION" ]
    then
        echo "Peer ${peer[0]} not located"
    else
        echo "Peer ${peer[0]} located in $FOUND_PEER_REGION"
        propagateRoute $FOUND_PEER_REGION ${peer[0]} ${peer[1]} $PRIMARY_REGION $PRIMARY_TGW $PRIMARY_ATTACHED_CIDR $PRIMARY_TGW_VPCS
    fi
    echo "(Complete) Primary TGW Peer : ${peer[0]} (attachment) ${peer[1]}"
done    

