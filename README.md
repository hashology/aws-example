# Kubernetes & Terraform

Terraform modules to create the EKS cluster and related infrastructure.

##### Requirements

* AWS CLI installed and configured 
* Kubectl installed 
* AWS IAM Authenticator
* Terraform >= 0.13.5

#### Creating a new environment

1. Create a new folder under terraform/environments/[aws region]
2. Use `us-west-1/eks-1` as a source of inspiration in VPC layout, configs, etc.
3. Within the directory with main.tf initialize terraform (terraform init)
 * Make sure your backend bucket is in the format `[region]/[environment name]`
4. Deploy infrastructure (terraform apply)

#### Sample EKS Terraform Configuration

```
module "eks" {
  source = "../../../../modules/eks"

  subnet_ids    = module.network.eks_subnet_ids

  public_api_whitelist = [
    "99.30.124.235/32" #gateway vpn ip
  ]

  node_groups = [
    {
      name = "client_X_86"
      node_count    = 1
      ami_type = "AL2_x86_64"
      instance_type = "t3.medium"
      disk_size = 20
      labels = {
        client = module.configuration.client
        environment = module.configuration.environment
        arch = "x86_64"
      }
    },
    {
      name = "client_Y_ARM"
      node_count    = 1
      ami_type = "AL2_ARM_64"
      instance_type = "a1.medium"
      disk_size = 20,
      labels = {
        client = module.configuration.client
        environment = module.configuration.environment
        arch = "ARM_64"
      }
    }
  ]

  tags = {
    application = module.configuration.application
    environment = module.configuration.environment
    client      = module.configuration.client
    owner       = module.configuration.owner
  }
}
```

#### Sample Terraform Output

```
bucket_access_id = <redacted>
bucket_access_secret = <redacted>
db_address = internal-poc-rds.cjiuqjr2iqrx.us-west-2.rds.amazonaws.com
db_password = <redacted>
redis_address = internal-poc.e8xyiy.ng.0001.usw2.cache.amazonaws.com
service-account-arn = arn:aws:iam::999999999999:role/internal-poc-eks-alb-ingress-controller
```

### Connecting to the EKS cluster

The cluster makes use of IAM to determine whether to allow a user to connect to and manage the system.

Download `aws-iam-authenticator` and makes sure it's on your PATH.

Instructions can be found:

https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html

#### Retrieve Kubectl config post-setup and set the context

Execute the following:

```
aws eks --region <region> update-kubeconfig --name <eks cluster name>
kubectl config get-contexts
kubectl config set-context <context-name>
```

#### One-time Cluster Setup

```
cd k8s/cluster-config
./prepare.sh <eks cluster name> <dns zone for auto updates> <path to tf directory>
#EX: ./prepare.sh internal-poc hashology.io ../terraform/clients/us-west-1/eks-1
```

##### Setup Calico Alias

The calicoctl command is deployed as a pod within Kubernetes. To use it conveniently, you can setup and alias

```
alias calicoctl="kubectl exec -ti -n kube-system calicoctl -- /calicoctl"
```


Validate the controllers are running

```
kubectl get pods -n kube-system

NAME                                      READY   STATUS    RESTARTS   AGE
alb-ingress-controller-859444ffdc-d9znt   1/1     Running   0          3h22m
aws-node-csgm8                            1/1     Running   0          4h10m
aws-node-hbp6q                            1/1     Running   0          4h22m
coredns-5946c5d67c-jq2gl                  1/1     Running   0          4h26m
coredns-5946c5d67c-p9rtq                  1/1     Running   0          4h26m
external-dns-7b6d598846-7vwws             1/1     Running   0          92m
kube-proxy-h8q5n                          1/1     Running   0          4h22m
kube-proxy-pr5km                          1/1     Running   0          4h10m
```


#### Allow an IAM user access to the K8S API

_**Must be done by user that created the cluster or one that already has been added as an admin**_

```
cd cluster-config
./add-user.sh <IAM username>
```


#### Example K8S Deployment

The below K8S manifests shows a few features of K8S.

1. The containers will only run on K8S nodes where the label `arch` is set to `x86_64` by the `nodeSelector` parameter.
    - This label is set during the node group creation in Terraform. (See the sample EKS definition above)
    - This allows us to slice up an EKS cluster for specific purposes such as running mixed-architecture clusters or restricting Deployments to specific nodes for contractual or security or other reasons.
2. The two annotations under the Ingress `alb.ingress.kubernetes.io/scheme` and `kubernetes.io/ingress.class` allows K8S to auto build load balancers for each Ingress.
    - This is needed only if you want traffic to come in from the "outside" to a Service within K8S.
3. Each Service is accessible by it's `metadata.name` property. In this case, any other resource within the same namespace can access the echoserver Deployment by going to http://echoserver. This will be resolved by coredns within the cluster.
    - A snazzy feature about Services is that you can even define a Service for a resource outside of K8S. An example of this is that in the ConfigMap, we can have a standard URL for ElasticSearch that points to https://elasticsearch:443 (managed ElasticSearch) and this will in turn look up the Service definition that resolves to the real DNS/Hostname.
4. The `host` property defined on the Ingress will be used to auto create public DNS records, by the external-dns controller if the root of the domain matches what was provided to the `./provide.sh` script under "*One-time ALB Ingress and External DNS Controller Setup*"
5. K8S significantly reduces our complexity and reliance on Terraform. Terraform is good but it only goes so far into managing the world of containers. K8S automates a lot resources by deploying controllers (as a one time effort), and defining annotations on resources.


```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echoserver
spec:
  replicas: 1
  selector:
    matchLabels:
      app: echoserver
  template:
    metadata:
      labels:
        app: echoserver
    spec:
      containers:
      - image: gcr.io/google_containers/echoserver:1.4
        imagePullPolicy: Always
        name: echoserver
        ports:
        - containerPort: 8080
      nodeSelector:
        arch: x86_64
---
apiVersion: v1
kind: Service
metadata:
  name: echoserver
spec:
  ports:
    - port: 80
      targetPort: 8080
      protocol: TCP
  type: NodePort
  selector:
    app: echoserver
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  annotations:
    alb.ingress.kubernetes.io/scheme: internet-facing
    kubernetes.io/ingress.class: alb
  name: echoserver
spec:
  rules:
  - host: internal-echoserver.hashology.io
    http: &echoserver_root
      paths:
      - backend:
          serviceName: echoserver
          servicePort: 80
        path: /
```

#### AWS Console Screenshots

##### EKS Cluster

![EKS](k8s/supplemental/eks.PNG)

##### Kibana / Visualization

![Kibana](k8s/supplemental/kibana.PNG)

##### EKS Node Group Labels

![EKS NG LABELS](k8s/supplemental/eks-nodegroup-labels.PNG)

##### Load Balancer

![LOAD BALANCER](k8s/supplemental/load-balancer.PNG)

##### Route 53

![ROUTE 53](k8s/supplemental/route-53.PNG)
