#!/bin/bash

##  $1 = cluster name
##  $2 = dns zone for auto-adding public DNS records
##  $3 = path to where .terraform directory exists

if [ -z "$1" ]; then
    echo "Cluster name not provided is empty"
    exit 1
fi

if [ -z "$2" ]; then
    echo "DNS Zone not provided"
    exit 1
fi

if [ ! -f "$3/.terraform/terraform.tfstate" ]; then
    echo "TF Sate file $3/.terraform/terraform.tfstate not found"
    exit 1
fi

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#Deploy RBAC and service account
echo -e "${GREEN}Deploying RBAC and Service Account${NC}"
kubectl apply -f 1.17-rbac-role.yaml
pushd $3
export SERVICE_ARN=$(terraform output | grep service-account-arn | cut -d '=' -f 2- | tr -d '[:space:]')
echo $SERVICE_ARN
popd
kubectl annotate serviceaccount --overwrite=true -n kube-system alb-ingress-controller eks.amazonaws.com/role-arn=$SERVICE_ARN
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed RBAC and Service Account${NC}"
else
  echo -e "${RED}Failed Deploying RBAC and Service Account${NC}"
  exit 1
fi

#Deploy ALB ingress controller
echo -e "${GREEN}Deploying ALB Ingress Controller${NC}"
TEMPFILE=$(mktemp)
cp 1.17-alb-ingress-controller.yaml $TEMPFILE
sed -i "s/CLUSTER_NAME/$1/g" $TEMPFILE
kubectl apply -f $TEMPFILE
rm $TEMPFILE
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed ALB Ingress Controller${NC}"
else
  echo -e "${RED}Failed Deploying ALB Ingress Controller${NC}"
  exit 1
fi

#Deploy External DNS
echo -e "${GREEN}Deploying External DNS Controller${NC}"
TEMPFILE=$(mktemp)
cp 1.17-external-dns.yaml $TEMPFILE
sed -i "s/DNS_ZONE/$2/g" $TEMPFILE
sed -i "s|SERVICE_ARN|$SERVICE_ARN|g" $TEMPFILE
kubectl apply -f $TEMPFILE
rm $TEMPFILE
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed External DNS Controller${NC}"
else
  echo -e "${RED}Failed Deploying External DNS Controller${NC}"
  exit 1
fi

# #Install EBS Driver for PersistentVolumeClaim
# echo -e "${GREEN}Deploying EBS Driver for PersistentVolumeClaim${NC}"
# kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"
# if [ $? -eq 0 ];
# then
#   echo -e "${GREEN}Deployed EBS Driver for PersistentVolumeClaim${NC}"
# else
#   echo -e "${RED}Failed Deploying EBS Driver for PersistentVolumeClaim${NC}"
#   exit 1
# fi

# #Install EBS Storage Class
# echo -e "${GREEN}Deploying EBS EXT4 Storage Class${NC}"
# kubectl apply -f 1.17-ebs-storage-class.yaml
# if [ $? -eq 0 ];
# then
#   echo -e "${GREEN}Deployed EBS EXT4 Storage Class${NC}"
# else
#   echo -e "${RED}Failed Deploying EBS EXT4 Storage Class${NC}"
#   exit 1
# fi

#Install AutoScaler Controller
echo -e "${GREEN}Deploying AutoScaler Controller${NC}"
TEMPFILE=$(mktemp)
REGION=$(echo "$1" | sed -r 's|hashology-(.*)-eks.*|\1|')
QUERY="AutoScalingGroups[].Tags[?Key==\`eks:nodegroup-name\` && Value==\`$1-default\`].ResourceId"
ASG_RESOURCE=$(aws --region $REGION autoscaling describe-auto-scaling-groups --query "$QUERY" --output text)
cp 1.17-autoscaler.yaml $TEMPFILE
sed -i "s/ASG_RESOURCE/$ASG_RESOURCE/g" $TEMPFILE
kubectl apply -f $TEMPFILE
rm $TEMPFILE
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed AutoScaler Controller${NC}"
else
  echo -e "${RED}Failed Deploying AutoScaler Controller${NC}"
  exit 1
fi

#Install External Secrets CRD
echo -e "${GREEN}Deploying External Secrets CRD${NC}"
kubectl apply -f external-secrets
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed External Secrets CRD${NC}"
else
  echo -e "${RED}Failed Deploying External Secrets CRD${NC}"
  exit 1
fi

#Install K8S Cloudwatch Adapter
echo -e "${GREEN}Deploying K8S Cloudwatch Adapter${NC}"
kubectl apply -f 1.17-k8s-cloudwatch-adapter.yaml
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed K8S Cloudwatch Adapter${NC}"
else
  echo -e "${RED}Failed Deploying K8S Cloudwatch Adapter${NC}"
  exit 1
fi

#Install Kube State Metrics
echo -e "${GREEN}Deploying Kube State Metrics${NC}"
kubectl apply -f kube-state-metrics
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed Kube State Metrics${NC}"
else
  echo -e "${RED}Failed Deploying Kube State Metrics${NC}"
  exit 1
fi

#Install MetricBeat
echo -e "${GREEN}Deploying MetricBeat${NC}"
kubectl apply -f 1.17-metricbeat-kubernetes.yaml
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed MetricBeat${NC}"
else
  echo -e "${RED}Failed Deploying MetricBeat${NC}"
  exit 1
fi

#Install Calico
echo -e "${GREEN}Deploying Calico${NC}"
kubectl apply -f 1.17-calico.yaml
kubectl apply -f 1.17-calico-arm64.yaml
kubectl apply -f 1.17-calicoctl.yaml
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deployed Calico${NC}"
else
  echo -e "${RED}Failed Deploying Calico${NC}"
  exit 1
fi

