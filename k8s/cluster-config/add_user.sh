#!/bin/bash

##  $1 = IAM Username

if [ -z "$1" ]; then
    echo "IAM username provided is empty"
    exit 1
fi

TEMPFILE=$(mktemp)
TEMPFILE_ADD=$(mktemp)

USER_ARN=$(aws iam get-user --user-name $1 --query 'User.Arn' --output text)
if [ $? -ne 0 ]; then
    exit 1
fi
kubectl get configmap -n kube-system aws-auth -o yaml > $TEMPFILE

if grep "mapUsers" $TEMPFILE
then
  cat << EOF > $TEMPFILE_ADD
    - userarn: $USER_ARN
      username: $1
      groups:
        - system:masters
EOF
    sed -i '' -e "/mapUsers:/r $TEMPFILE_ADD" $TEMPFILE
else
  cat << EOF > $TEMPFILE_ADD
  mapUsers: |
    - userarn: $USER_ARN
      username: $1
      groups:
        - system:masters
EOF
    sed -i '' -e "/^data:/r $TEMPFILE_ADD" $TEMPFILE
fi
rm $TEMPFILE_ADD
cat $TEMPFILE
kubectl apply -f $TEMPFILE
rm $TEMPFILE
