#!/bin/bash

##  $1 = cluster name
##  $2 = dns zone for auto-adding public DNS records
##  $3 = path to where .terraform directory exists

if [ -z "$1" ]; then
    echo "Cluster name not provided is empty"
    exit 1
fi

if [ -z "$2" ]; then
    echo "DNS Zone not provided"
    exit 1
fi

if [ ! -f "$3/.terraform/terraform.tfstate" ]; then
    echo "TF Sate file $3/.terraform/terraform.tfstate not found"
    exit 1
fi

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#Deploy RBAC and service account
echo -e "${GREEN}Deleting RBAC and Service Account${NC}"
kubectl delete --ignore-not-found -f 1.17-rbac-role.yaml
pushd $3
export SERVICE_ARN=$(terraform output | grep service-account-arn | cut -d '=' -f 2- | tr -d '[:space:]')
echo $SERVICE_ARN
popd
#kubectl annotate serviceaccount --overwrite=true -n kube-system alb-ingress-controller eks.amazonaws.com/role-arn=$SERVICE_ARN
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted RBAC and Service Account${NC}"
else
  echo -e "${RED}Failed Deleting RBAC and Service Account${NC}"
  exit 1
fi

#Deploy ALB ingress controller
echo -e "${GREEN}Deleting ALB Ingress Controller${NC}"
TEMPFILE=$(mktemp)
cp 1.17-alb-ingress-controller.yaml $TEMPFILE
sed -i "s/CLUSTER_NAME/$1/g" $TEMPFILE
kubectl delete --ignore-not-found -f $TEMPFILE
rm $TEMPFILE
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted ALB Ingress Controller${NC}"
else
  echo -e "${RED}Failed Deleting ALB Ingress Controller${NC}"
  exit 1
fi

#Deploy External DNS
echo -e "${GREEN}Deleting External DNS Controller${NC}"
TEMPFILE=$(mktemp)
cp 1.17-external-dns.yaml $TEMPFILE
sed -i "s/DNS_ZONE/$2/g" $TEMPFILE
sed -i "s|SERVICE_ARN|$SERVICE_ARN|g" $TEMPFILE
kubectl delete --ignore-not-found -f $TEMPFILE
rm $TEMPFILE
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted External DNS Controller${NC}"
else
  echo -e "${RED}Failed Deleting External DNS Controller${NC}"
  exit 1
fi

# #Install EBS Driver for PersistentVolumeClaim
# echo -e "${GREEN}Deleting EBS Driver for PersistentVolumeClaim${NC}"
# kubectl delete --ignore-not-found -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"
# if [ $? -eq 0 ];
# then
#   echo -e "${GREEN}Deleted EBS Driver for PersistentVolumeClaim${NC}"
# else
#   echo -e "${RED}Failed Deleting EBS Driver for PersistentVolumeClaim${NC}"
#   exit 1
# fi

# #Install EBS Storage Class
# echo -e "${GREEN}Deleting EBS EXT4 Storage Class${NC}"
# kubectl delete --ignore-not-found -f 1.17-ebs-storage-class.yaml
# if [ $? -eq 0 ];
# then
#   echo -e "${GREEN}Deleted EBS EXT4 Storage Class${NC}"
# else
#   echo -e "${RED}Failed Deleting EBS EXT4 Storage Class${NC}"
#   exit 1
# fi

#Install AutoScaler Controller
echo -e "${GREEN}Deleting AutoScaler Controller${NC}"
TEMPFILE=$(mktemp)
cp 1.17-autoscaler.yaml $TEMPFILE
sed -i "s/CLUSTER_NAME/$1/g" $TEMPFILE
kubectl delete --ignore-not-found -f $TEMPFILE
rm $TEMPFILE
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted AutoScaler Controller${NC}"
else
  echo -e "${RED}Failed Deleting AutoScaler Controller${NC}"
  exit 1
fi

#Install External Secrets CRD
echo -e "${GREEN}Deleting External Secrets CRD${NC}"
kubectl delete --ignore-not-found -f external-secrets
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted External Secrets CRD${NC}"
else
  echo -e "${RED}Failed Deleting External Secrets CRD${NC}"
  exit 1
fi

#Install K8S Cloudwatch Adapter
echo -e "${GREEN}Deleting K8S Cloudwatch Adapter${NC}"
kubectl delete --ignore-not-found -f 1.17-k8s-cloudwatch-adapter.yaml
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted K8S Cloudwatch Adapter${NC}"
else
  echo -e "${RED}Failed Deleting K8S Cloudwatch Adapter${NC}"
  exit 1
fi

#Install Kube State Metrics
echo -e "${GREEN}Deleting Kube State Metrics${NC}"
kubectl delete --ignore-not-found -f kube-state-metrics
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted Kube State Metrics${NC}"
else
  echo -e "${RED}Failed Deleting Kube State Metrics${NC}"
  exit 1
fi

#Install MetricBeat
echo -e "${GREEN}Deleting MetricBeat${NC}"
kubectl delete --ignore-not-found -f 1.17-metricbeat-kubernetes.yaml
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Deleted MetricBeat${NC}"
else
  echo -e "${RED}Failed Deleting  MetricBeat${NC}"
  exit 1
fi

#Install Calico
echo -e "${GREEN}Delete Calico${NC}"
kubectl delete -f 1.17-calico.yaml
kubectl delete -f 1.17-calico-arm64.yaml
kubectl delete -f 1.17-calicoctl.yaml
if [ $? -eq 0 ];
then
  echo -e "${GREEN}Delete Calico${NC}"
else
  echo -e "${RED}Failed Delete Calico${NC}"
  exit 1
fi

