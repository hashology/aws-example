#!/bin/bash

LOG_TYPE="database"
NEW_MASTER_PASSWORD="backup2020"
OFF_SITE_BACKUP_LAG_DAYS="7"
LOG_TO_ECHO=true
MAC=$(curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs | head -n 1)
VPC=$(curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs/${MAC}/vpc-id)
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
SUBNET=$(curl -s http://169.254.169.254/latest/meta-data/network/interfaces/macs/${MAC}/subnet-id)
DB_SEC_GROUP=$(aws ec2 describe-security-groups --region $REGION --filters "Name=vpc-id,Values=$VPC" --query 'SecurityGroups[?IpPermissions[?ToPort==`3306` && contains(IpRanges[].CidrIp, `10.0.0.0/8`)]].{GroupId: GroupId, GroupName: GroupName}' --output text | cut -f1)
DB_SUBNET_GROUP=$(aws rds describe-db-subnet-groups --region $REGION | jq -r ".DBSubnetGroups[] | select (.VpcId == \"$VPC\") | .DBSubnetGroupName")
SNAPSHOT_START_TIMEGATE=$(date -d "1 days ago" '+%Y-%m-%d')
SNAPSHOT_END_TIMEGATE=$(date -d "7 days ago 23:59:59" '+%Y-%m-%dT%H:%M:%S')

SNAPSHOTS=($(aws rds describe-db-snapshots --region $REGION --snapshot-type automated --query "DBSnapshots[?SnapshotCreateTime>='$SNAPSHOT_START_TIMEGATE' && SnapshotCreateTime<='$SNAPSHOT_END_TIMEGATE'].[DBSnapshotIdentifier,SnapshotCreateTime]" --filters 'Name=engine,Values=mysql' --output text | tr '\t' ,))

rm -rf *.xz

log() {
  # logging
  environment=$1
  log_line=$2
  local_log=${3:-false}

  if [ "$local_log" = true ] ; then
    echo "[${environment}]: $log_line"
    return
  fi

  aws logs create-log-stream --log-group-name /hashology/backups --log-stream-name "$environment-$LOG_TYPE" --region $REGION 2>&1 >/dev/null || true
  sequenceToken=$(aws logs describe-log-streams --log-group-name /hashology/backups --log-stream-name-prefix "$environment-$LOG_TYPE" --region $REGION |  sed -n 's|.*"uploadSequenceToken"\s*:\s*"\([^"]*\)".*|\1|p')
  if [ ! -z "${sequenceToken}" ]; then
    sequenceToken="--sequence-token $sequenceToken"
  fi
  sequenceToken=$(aws logs put-log-events --log-group-name /hashology/backups --log-stream-name "$environment-$LOG_TYPE" --region $REGION --log-events "timestamp=$(date +%s%3N),message='[${environment}]: $log_line'" ${sequenceToken} | sed -n 's|.*"nextSequenceToken"\s*:\s*"\([^"]*\)".*|\1|p')
}

for snap in "${SNAPSHOTS[@]}"
do
  IFS=','
  read -a array <<< "$snap"
  unset IFS

  SNAP_NAME="${array[0]}"
  SNAP_TIMESTAMP="${array[1]}"
  RDS_NAME=${SNAP_NAME#"rds:"}
  RDS_NAME=$(echo "$RDS_NAME" | sed -E 's/-[[:digit:]]{4}(-[[:digit:]]{2})+//g')
  SNAPSHOT_FORMATTED=${SNAP_NAME#"rds:"}
  SNAPSHOT_CREATE_TIME=$(aws rds describe-db-snapshots --region $REGION --db-snapshot-identifier "$SNAP_NAME" --query "DBSnapshots[*].SnapshotCreateTime" --output text)

  if [[ "$SNAP_NAME" =~ (migration-tool|streamline|datawarehouse|secondary|uat|stage|impact-evaluation|sync) ]]; then
    continue
  fi

  aws s3 ls "s3://hashology-${REGION}-rds/${RDS_NAME}_${SNAPSHOT_CREATE_TIME}.xz"
  if [[ $? -eq 0 ]]; then
    log $RDS_NAME "Backup already exists for: $SNAPSHOT_FORMATTED. Skipping..." ${LOG_TO_ECHO}
    continue
  fi

  #Restore snapshot
  log $RDS_NAME "Restoring Temporary RDS Copy: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}
  aws rds restore-db-instance-from-db-snapshot --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite" --db-snapshot-identifier "$SNAP_NAME" --db-subnet-group-name "$DB_SUBNET_GROUP" --db-instance-class "db.m5.large" --vpc-security-group-ids "$DB_SEC_GROUP" --storage-type "gp2" >/dev/null
  aws rds wait db-instance-available --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite"
  sleep 60 #never really done are we? - give some extra wait time
  log $RDS_NAME "Restored Temporary RDS Copy: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}

  #Get master username and reset password
  log $RDS_NAME "Resetting Temporary RDS Master Password: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}
  MASTER_USERNAME=$(aws rds describe-db-instances --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite" | jq -r '.DBInstances[].MasterUsername')
  aws rds modify-db-instance --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite" --master-user-password "$NEW_MASTER_PASSWORD" >/dev/null
  sleep 60 #status takes a while to move to the modifying state
  aws rds wait db-instance-available --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite"
  sleep 60 #never really done are we? - give some extra wait time
  log $RDS_NAME "Reset Temporary RDS Master Password: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}

  #Get RDS Endpoint
  ENDPOINT=$(aws rds describe-db-instances --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite" | jq -r '.DBInstances[].Endpoint.Address')

  log $RDS_NAME "Extracting Data From Temporary RDS: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}
  DB_SCHEMAS=($(mysql -h $ENDPOINT -u $MASTER_USERNAME -p$NEW_MASTER_PASSWORD -s -N -e "show databases;" | grep -Ev 'information_schema|innodb|mysql|tmp|performance_schema|sys'))
  DB_SCHEMAS_JOINED=$(printf " %s" "${DB_SCHEMAS[@]}")

  mysqldump -h $ENDPOINT -u $MASTER_USERNAME -p$NEW_MASTER_PASSWORD --databases ${DB_SCHEMAS_JOINED} | xz -T 0 -6 > "/tmp/${RDS_NAME}_${SNAPSHOT_CREATE_TIME}.xz"

  log $RDS_NAME "Extracted Data From Temporary RDS: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}

  for file in *.xz
  do
    log $RDS_NAME "Uploading Compressed Data From Temporary RDS: ~/${RDS_NAME}_${DB_SCHEMAS[0]}_${SNAPSHOT_CREATE_TIME}.xz" ${LOG_TO_ECHO}
    aws s3 cp "/tmp/${RDS_NAME}_${SNAPSHOT_CREATE_TIME}.xz" "s3://hashology-${REGION}-rds/" --acl bucket-owner-full-control
    log $RDS_NAME "Uploaded Compressed Data From Temporary RDS: ~/${RDS_NAME}_${DB_SCHEMAS[0]}_${SNAPSHOT_CREATE_TIME}.xz" ${LOG_TO_ECHO}
  done

  log $RDS_NAME "Removing Temporary RDS: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}
  aws rds delete-db-instance --region $REGION --db-instance-identifier "${SNAPSHOT_FORMATTED}-offsite" --skip-final-snapshot --delete-automated-backups >/dev/null
  log $RDS_NAME "Removed Temporary RDS: ${SNAPSHOT_FORMATTED}-offsite" ${LOG_TO_ECHO}

  rm -rf *.xz
done
