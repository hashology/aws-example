#!/bin/bash

REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
BUCKETS=($(aws --region us-east-1 dynamodb scan --table bucket_region  --filter-expression 'r = :r' --expression-attribute-values "{ \":r\": {\"S\": \"${REGION}\"}}" --query 'Items[*].b.S' --output text))

log() {
  # logging
  environment=$1
  log_line=$2
  local_log=${3:-false}

  if [ "$local_log" = true ] ; then
    echo "[${environment}]: $log_line"
    return
  fi

  aws logs create-log-stream --log-group-name /hashology/backups --log-stream-name "$environment-$LOG_TYPE" --region $REGION 2>&1 >/dev/null || true
  sequenceToken=$(aws logs describe-log-streams --log-group-name /hashology/backups --log-stream-name-prefix "$environment-$LOG_TYPE" --region $REGION |  sed -n 's|.*"uploadSequenceToken"\s*:\s*"\([^"]*\)".*|\1|p')
  if [ ! -z "${sequenceToken}" ]; then
    sequenceToken="--sequence-token $sequenceToken"
  fi
  sequenceToken=$(aws logs put-log-events --log-group-name /hashology/backups --log-stream-name "$environment-$LOG_TYPE" --region $REGION --log-events "timestamp=$(date +%s%3N),message='[${environment}]: $log_line'" ${sequenceToken} | sed -n 's|.*"nextSequenceToken"\s*:\s*"\([^"]*\)".*|\1|p')
}

for bucket in "${BUCKETS[@]}"
do 
  echo "aws s3 sync s3://$bucket s3://$bucket-backup"
  aws s3 sync s3://$bucket s3://$bucket-backup --acl bucket-owner-full-control
done