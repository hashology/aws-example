resource "aws_ec2_transit_gateway" "tgw" {
  amazon_side_asn = var.asn
  tags = var.tags
}

output "id" {
  value = aws_ec2_transit_gateway.tgw.id
}