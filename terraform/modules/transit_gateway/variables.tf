variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "asn" {
  default = 64512
}