variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "region" {
  default = "us-east-1"
}

variable "transit_gateway_id" {
  
}

variable "peer_transit_gateway_id" {
  
}