
resource "aws_ec2_transit_gateway_peering_attachment" "peer" {
  peer_region             = var.region
  peer_transit_gateway_id = var.peer_transit_gateway_id
  transit_gateway_id      = var.transit_gateway_id

  tags = var.tags
}

output "attachment_id" {
  value = aws_ec2_transit_gateway_peering_attachment.peer.id
}