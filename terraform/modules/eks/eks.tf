resource "aws_iam_role" "hashology" {
  name = var.tags["Name"]

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "eks.amazonaws.com",
          "ec2.amazonaws.com"
          ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

}

resource "aws_iam_policy" "hashology-worker" {
  name = "${var.tags["Name"]}-worker"
  description = "${var.tags["Name"]}-worker"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        },
        {
          "Effect": "Allow",
          "Action": [
            "ec2:AttachVolume",
            "ec2:CreateSnapshot",
            "ec2:CreateTags",
            "ec2:CreateVolume",
            "ec2:DeleteSnapshot",
            "ec2:DeleteTags",
            "ec2:DeleteVolume",
            "ec2:DescribeAvailabilityZones",
            "ec2:DescribeInstances",
            "ec2:DescribeSnapshots",
            "ec2:DescribeTags",
            "ec2:DescribeVolumes",
            "ec2:DescribeVolumesModifications",
            "ec2:DetachVolume",
            "ec2:ModifyVolume"
          ],
          "Resource": "*"
        },
        {
          "Effect": "Allow",
          "Action": [
            "acm:DescribeCertificate",
            "acm:ListCertificates",
            "acm:GetCertificate"
          ],
          "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ecr:BatchCheckLayerAvailability",
                "ecr:BatchGetImage",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetAuthorizationToken"
            ],
            "Resource": "*"
        },
        {
            "Action": [
              "ec2:AuthorizeSecurityGroupIngress",
              "ec2:CreateSecurityGroup",
              "ec2:CreateTags",
              "ec2:DeleteTags",
              "ec2:DeleteSecurityGroup",
              "ec2:DescribeAccountAttributes",
              "ec2:DescribeAddresses",
              "ec2:DescribeInstances",
              "ec2:DescribeInstanceStatus",
              "ec2:DescribeInternetGateways",
              "ec2:DescribeNetworkInterfaces",
              "ec2:DescribeSecurityGroups",
              "ec2:DescribeSubnets",
              "ec2:DescribeTags",
              "ec2:DescribeVpcs",
              "ec2:ModifyInstanceAttribute",
              "ec2:ModifyNetworkInterfaceAttribute",
              "ec2:RevokeSecurityGroupIngress",
              "ecr:GetDownloadUrlForLayer",
              "ecr:GetAuthorizationToken"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "autoscaling:DescribeTags",
                "autoscaling:DescribeLaunchConfigurations",
                "ec2:DescribeLaunchTemplateVersions",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "autoscaling:UpdateAutoScalingGroup",
                "autoscaling:CreateOrUpdateTags"
            ],
            "Resource": ["*"]
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:GetMetricData",
                "cloudwatch:PutMetricData"
            ],
            "Resource": "*"
        },
    {
      "Effect": "Allow",
      "Action": [
        "elasticloadbalancing:AddListenerCertificates",
        "elasticloadbalancing:AddTags",
        "elasticloadbalancing:CreateListener",
        "elasticloadbalancing:CreateLoadBalancer",
        "elasticloadbalancing:CreateRule",
        "elasticloadbalancing:CreateTargetGroup",
        "elasticloadbalancing:DeleteListener",
        "elasticloadbalancing:DeleteLoadBalancer",
        "elasticloadbalancing:DeleteRule",
        "elasticloadbalancing:DeleteTargetGroup",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:DescribeListenerCertificates",
        "elasticloadbalancing:DescribeListeners",
        "elasticloadbalancing:DescribeLoadBalancers",
        "elasticloadbalancing:DescribeLoadBalancerAttributes",
        "elasticloadbalancing:DescribeRules",
        "elasticloadbalancing:DescribeSSLPolicies",
        "elasticloadbalancing:DescribeTags",
        "elasticloadbalancing:DescribeTargetGroups",
        "elasticloadbalancing:DescribeTargetGroupAttributes",
        "elasticloadbalancing:DescribeTargetHealth",
        "elasticloadbalancing:ModifyListener",
        "elasticloadbalancing:ModifyLoadBalancerAttributes",
        "elasticloadbalancing:ModifyRule",
        "elasticloadbalancing:ModifyTargetGroup",
        "elasticloadbalancing:ModifyTargetGroupAttributes",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:RemoveListenerCertificates",
        "elasticloadbalancing:RemoveTags",
        "elasticloadbalancing:SetIpAddressType",
        "elasticloadbalancing:SetSecurityGroups",
        "elasticloadbalancing:SetSubnets",
        "elasticloadbalancing:SetWebACL"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "secretsmanager:GetRandomPassword",
        "secretsmanager:GetResourcePolicy",
        "secretsmanager:GetSecretValue",
        "secretsmanager:DescribeSecret",
        "secretsmanager:ListSecretVersionIds",
        "secretsmanager:ListSecrets"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:CreateServiceLinkedRole",
        "iam:GetServerCertificate",
        "iam:ListServerCertificates"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "cognito-idp:DescribeUserPoolClient"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "waf-regional:GetWebACLForResource",
        "waf-regional:GetWebACL",
        "waf-regional:AssociateWebACL",
        "waf-regional:DisassociateWebACL",
        "waf:GetWebACL",
				"wafv2:GetWebACL",
				"wafv2:GetWebACLForResource",
				"wafv2:AssociateWebACL",
				"wafv2:DisassociateWebACL",
				"shield:DescribeProtection",
				"shield:GetSubscriptionState",
				"shield:DeleteProtection",
				"shield:CreateProtection",
				"shield:DescribeSubscription",
				"shield:ListProtections"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "tag:GetResources",
        "tag:TagResources"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "waf:GetWebACL"
      ],
      "Resource": "*"
    },
    {
        "Effect": "Allow",
        "Action": [
            "s3:GetObject",
            "s3:PutObject",
            "s3:ListBucket"
        ],
        "Resource": [
        %{ for bucket in var.s3_access_buckets }
          "arn:aws:s3:::${bucket}"
        %{ endfor }
        ]
    }
]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "hashology-dev-worker-policy" {
  policy_arn = aws_iam_policy.hashology-worker.arn
  role       = aws_iam_role.hashology.name
}

resource "aws_iam_role_policy_attachment" "hashology-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.hashology.name
}

resource "aws_iam_role_policy_attachment" "hashology-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.hashology.name
}
resource "aws_iam_role_policy_attachment" "hashology-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.hashology.name
}

resource "aws_iam_role_policy_attachment" "hashology-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.hashology.name
}

resource "aws_iam_role_policy_attachment" "hashology-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.hashology.name
}

data "tls_certificate" "cluster" {
  url = aws_eks_cluster.hashology.identity.0.oidc.0.issuer
}

resource "aws_iam_openid_connect_provider" "hashology" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.cluster.certificates.0.sha1_fingerprint]
  url             = aws_eks_cluster.hashology.identity.0.oidc.0.issuer
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "hashology-assume" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(aws_iam_openid_connect_provider.hashology.url, "https://", "")}:sub"
      values   = ["system:serviceaccount:kube-system:aws-node", "system:serviceaccount:kube-system:external-dns", "system:serviceaccount:kube-system:alb-ingress-controller"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.hashology.arn]
      type        = "Federated"
    }
  }
}

resource "aws_iam_policy" "r53" {
  name        = "${var.tags["Name"]}-route53"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "route53:ChangeResourceRecordSets"
      ],
      "Resource": [
        "arn:aws:route53:::hostedzone/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ListHostedZones",
        "route53:ListResourceRecordSets"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}


resource "aws_iam_role" "hashology-svc-account" {
  assume_role_policy = data.aws_iam_policy_document.hashology-assume.json
  name               = "${var.tags["Name"]}-eks-alb-ingress-controller"
}

resource "aws_iam_role_policy_attachment" "hashology-attach" {
  role       = aws_iam_role.hashology-svc-account.name
  policy_arn = aws_iam_policy.r53.arn
}

resource "aws_iam_role_policy_attachment" "alb-svc-account" {
  role       = aws_iam_role.hashology-svc-account.name
  policy_arn = aws_iam_policy.hashology-worker.arn
}

resource "aws_eks_cluster" "hashology" {
  name     = var.tags["Name"]
  role_arn = aws_iam_role.hashology.arn
  version     = var.cluster_version

  vpc_config {
    subnet_ids              = var.subnet_ids
    endpoint_private_access = true
    endpoint_public_access  = length(var.public_api_whitelist) > 0 ? true : false
    public_access_cidrs = var.public_api_whitelist
  }

  tags = var.tags

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.hashology-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.hashology-AmazonEKSServicePolicy,
  ]
}

# On demand instance node group

data "template_file" "node_user_data" {
  count = length(var.node_groups)
  template = <<EOF
  #!/bin/bash
  set -ex

  #ADD CUSTOM TAG FOR CLUSTER AUTODISCOVERY 
  INSTANCE_ID=$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)
  REGION=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

  ASG_NAME=$(aws --region $REGION autoscaling describe-auto-scaling-groups | jq -r ".AutoScalingGroups[] | select (.Instances[].InstanceId | contains(\"$INSTANCE_ID\")) | .AutoScalingGroupName")

  NODE_GROUP_NAME=$( aws --region $REGION autoscaling describe-auto-scaling-groups | jq -r ".AutoScalingGroups[] | select (.Instances[].InstanceId | contains(\"$INSTANCE_ID\")) | .Tags[] | select(.Key==\"eks:nodegroup-name\") | .Value")

  aws --region $REGION autoscaling create-or-update-tags --tags ResourceId=$ASG_NAME,ResourceType=auto-scaling-group,Key=eks:nodegroup-name:$NODE_GROUP_NAME,Value=owned,PropagateAtLaunch=true

  #ADD SYSCTRL TO KUBELET
  KUBELET_CONFIG=/etc/kubernetes/kubelet/kubelet-config.json
  echo "$(jq '. + {allowedUnsafeSysctls: ["net.core.somaxconn", "kernel.msg*"]}' $KUBELET_CONFIG)" > $KUBELET_CONFIG
  systemctl stop kubelet
  EOF
  //  /etc/eks/bootstrap.sh ${aws_eks_cluster.hashology.name} --kubelet-extra-args '--node-labels=capacityType=ON_DEMAND,eks.amazonaws.com/nodegroup=${var.tags["Name"]}-${var.node_groups[count.index].name} --allowed-unsafe-sysctls=net.core.somaxconn,kernel.msg*'
  // removed - apparently bootstrap will call describe_cluster to get these?
  //--b64-cluster-ca $B64_CLUSTER_CA --apiserver-endpoint $API_SERVER_URL --dns-cluster-ip $K8S_CLUSTER_DNS_IP
}

data "template_cloudinit_config" "node_cloudinit_config" {
  count = length(var.node_groups)
  base64_encode = true
  gzip = false
  part {
    content_type = "text/x-shellscript"
    content = data.template_file.node_user_data[count.index].template
  }
}

resource "aws_launch_template" "hashology" {
  count = length(var.node_groups)
  name = "${var.tags["Name"]}-${var.node_groups[count.index].name}-launch-template"
  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = var.node_groups[count.index].disk_size
    }
  }
  capacity_reservation_specification {
    capacity_reservation_preference = "open"
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      "kubernetes.io/cluster/${aws_eks_cluster.hashology.name}" = "owned"
      Name = "${var.tags["Name"]}-${var.node_groups[count.index].name}"
    }
  }
  update_default_version = true

//  ebs_optimized = false
  instance_type = var.node_groups[count.index].instance_type

  user_data = data.template_cloudinit_config.node_cloudinit_config[count.index].rendered
}

resource "aws_eks_node_group" "hashology" {
  count = length(var.node_groups)
  cluster_name    = aws_eks_cluster.hashology.name
  node_group_name = "${var.tags["Name"]}-${var.node_groups[count.index].name}"
  node_role_arn   = aws_iam_role.hashology.arn
  subnet_ids      = var.subnet_ids
  ami_type = var.node_groups[count.index].ami_type
  labels = lookup(var.node_groups[count.index], "labels", {})

  launch_template {
    name = aws_launch_template.hashology[count.index].name
    version = aws_launch_template.hashology[count.index].latest_version
  }

  scaling_config {
    desired_size = 1
    max_size     = var.node_groups[count.index].max_node_count
    min_size     = 1
  }
  
  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.hashology-dev-worker-policy,
    aws_iam_role_policy_attachment.hashology-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.hashology-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.hashology-AmazonEC2ContainerRegistryReadOnly,
  ]

  tags = {
    "kubernetes.io/nodegroup/${var.tags["Name"]}-${var.node_groups[count.index].name}" = "owned"
  }
}

# Spot instance node group

data "template_file" "spot_node_user_data" {
  count = length(var.spot_node_groups)
  template = <<EOF
  #!/bin/bash
  set -ex

  #ADD CUSTOM TAG FOR CLUSTER AUTODISCOVERY 
  INSTANCE_ID=$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)
  REGION=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

  ASG_NAME=$(aws --region $REGION autoscaling describe-auto-scaling-groups | jq -r ".AutoScalingGroups[] | select (.Instances[].InstanceId | contains(\"$INSTANCE_ID\")) | .AutoScalingGroupName")

  NODE_GROUP_NAME=$( aws --region $REGION autoscaling describe-auto-scaling-groups | jq -r ".AutoScalingGroups[] | select (.Instances[].InstanceId | contains(\"$INSTANCE_ID\")) | .Tags[] | select(.Key==\"eks:nodegroup-name\") | .Value")

  aws --region $REGION autoscaling create-or-update-tags --tags ResourceId=$ASG_NAME,ResourceType=auto-scaling-group,Key=eks:nodegroup-name:$NODE_GROUP_NAME,Value=owned,PropagateAtLaunch=true

  #ADD SYSCTRL TO KUBELET
  KUBELET_CONFIG=/etc/kubernetes/kubelet/kubelet-config.json
  echo "$(jq '. + {allowedUnsafeSysctls: ["net.core.somaxconn", "kernel.msg*"]}' $KUBELET_CONFIG)" > $KUBELET_CONFIG
  systemctl stop kubelet
  EOF
}

data "template_cloudinit_config" "spot_node_cloudinit_config" {
  count = length(var.spot_node_groups)
  base64_encode = true
  gzip = false
  part {
    content_type = "text/x-shellscript"
    content = data.template_file.spot_node_user_data[count.index].template
  }
}

resource "aws_launch_template" "hashology_spot" {
  count = length(var.spot_node_groups)
  name = "${var.tags["Name"]}-${var.spot_node_groups[count.index].name}-spot-launch-template"

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = var.spot_node_groups[count.index].disk_size
    }
  }
  capacity_reservation_specification {
    capacity_reservation_preference = "open"
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      "kubernetes.io/cluster/${aws_eks_cluster.hashology.name}" = "owned"
    }
  }
  update_default_version = true
//  ebs_optimized = false

  user_data = data.template_cloudinit_config.spot_node_cloudinit_config[count.index].rendered
}

resource "aws_eks_node_group" "hashology_spot" {
  count = length(var.spot_node_groups)
  capacity_type   = "SPOT"
  instance_types = var.spot_node_groups[count.index].instance_types
  cluster_name    = aws_eks_cluster.hashology.name
  node_group_name = "${var.tags["Name"]}-${var.spot_node_groups[count.index].name}"
  node_role_arn   = aws_iam_role.hashology.arn
  subnet_ids      = var.subnet_ids
  ami_type = var.spot_node_groups[count.index].ami_type
  labels = lookup(var.spot_node_groups[count.index], "labels", {})
  launch_template {
    name = aws_launch_template.hashology_spot[count.index].name
    version = aws_launch_template.hashology_spot[count.index].latest_version
  }

  scaling_config {
    desired_size = 1
    max_size     = var.spot_node_groups[count.index].max_node_count
    min_size     = 1
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.hashology-dev-worker-policy,
    aws_iam_role_policy_attachment.hashology-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.hashology-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.hashology-AmazonEC2ContainerRegistryReadOnly,
  ]
  tags = {
    "kubernetes.io/nodegroup/${var.tags["Name"]}-${var.spot_node_groups[count.index].name}" = "owned"
  }
}
output "service-account-arn" {
  value = aws_iam_role.hashology-svc-account.arn
}
output "endpoint" {
  value = aws_eks_cluster.hashology.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.hashology.certificate_authority[0].data
}
