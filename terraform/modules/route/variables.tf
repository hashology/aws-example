variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "route_table_id" {

}
variable "transit_gateway_id" {
  default = ""
}
variable "vpc_peering_connection_id" {
  default = ""
}
variable "cidr_block" {

}
