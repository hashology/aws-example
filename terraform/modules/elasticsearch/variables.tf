variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}
variable "node_count" {
  type        = number
  description = "Number of nodes"
}
variable "subnet_ids" {
  type        = list(string)
  description = "List of subnet ids to place the environment's resources in. Set this to the base module's output variable `subnet_ids`"
}
variable "instance_class" {
  description = "The instance type of the worker node"
}
variable "master_instance_class" {
  description = "The instance type of the  master node"
}
variable "vpc_id" {}
variable "es_version" {}
variable "volume_size_gb" {}

variable "es_dns_name" {
  default = ""
}