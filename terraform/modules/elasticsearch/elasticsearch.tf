data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_security_group" "es" {
  name        = var.tags["Name"]
  vpc_id      = var.vpc_id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
}

resource "aws_elasticsearch_domain" "es" {
  domain_name           = var.tags["Name"]
  elasticsearch_version = var.es_version

  encrypt_at_rest {
    enabled = true
  }
  node_to_node_encryption {
    enabled = true
  }
  domain_endpoint_options {
    enforce_https = true
    tls_security_policy = "Policy-Min-TLS-1-0-2019-07"
  }
  ebs_options {
    ebs_enabled = true
    volume_size = var.volume_size_gb
  }
  cluster_config {
    instance_type = var.instance_class
    zone_awareness_enabled = true
    instance_count = var.node_count
    dedicated_master_enabled = true
    dedicated_master_count = 3
    dedicated_master_type = var.master_instance_class
  }

  vpc_options {
    subnet_ids = [var.subnet_ids[0], var.subnet_ids[1]]

    security_group_ids = [aws_security_group.es.id]
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.tags["Name"]}/*"
        }
    ]
}
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = merge(
    var.tags,
    {
      Domain = var.tags["Name"]
    }
  )
}

data "aws_route53_zone" "es" {
  count   = var.es_dns_name != "" ? 1 : 0
  name         = "${element(split(".", var.es_dns_name), length(split(".", var.es_dns_name)) -2)}.${element(split(".", var.es_dns_name), length(split(".", var.es_dns_name)) -1)}"
}

resource "aws_route53_record" "es" {
  count   = var.es_dns_name != "" ? 1 : 0
  zone_id = data.aws_route53_zone.es[0].zone_id
  name    = "${var.es_dns_name}."
  type    = "CNAME"
  ttl     = "60"
  records = [aws_elasticsearch_domain.es.endpoint]
}

output "es_endpoint" {
  value = aws_elasticsearch_domain.es.endpoint
}

output "es_kibana_endpoint" {
  value = aws_elasticsearch_domain.es.kibana_endpoint
}