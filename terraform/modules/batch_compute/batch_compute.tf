data "aws_region" "current" {}

resource "aws_iam_role" "ecs_instance_role" {
  name = "${var.name}_ecs_instance_role_${data.aws_region.current.name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_cw" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_rds" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_s3" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_kms" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::499602364333:policy/KMSFullAccess"
}
resource "aws_iam_role_policy_attachment" "ecs_instance_role_push_s3" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::499602364333:policy/push-offsite-backups"
}
resource "aws_iam_role_policy_attachment" "ecs_instance_role_dynamodb" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBReadOnlyAccess"
}

resource "aws_iam_instance_profile" "ecs_instance_role" {
  name = "${var.name}_ecs_instance_role_${data.aws_region.current.name}"
  role = aws_iam_role.ecs_instance_role.name
}

resource "aws_iam_role" "aws_batch_service_role" {
  name = "${var.name}_aws_batch_service_role_${data.aws_region.current.name}"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Effect": "Allow",
        "Principal": {
        "Service": "batch.amazonaws.com"
        }
    }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "aws_batch_instance_role_ec2" {
  role       = aws_iam_role.aws_batch_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "aws_batch_service_role" {
  role       = aws_iam_role.aws_batch_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole"
}

resource "aws_security_group" "sec_group" {
  name = "${var.name}_batch_sec_group_${data.aws_region.current.name}"
  vpc_id      = aws_vpc.batch_vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_rds" {
  name        = "${var.name}_batch_sec_group_${data.aws_region.current.name}_rds"
  vpc_id      =aws_vpc.batch_vpc.id

  ingress {
    description = "MySQL from VPC"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
}

resource "aws_vpc" "batch_vpc" {
  cidr_block = "10.1.0.0/16"
  tags = {
    Name = "batch"
  }
}


resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.batch_vpc.id
  tags  = {
    Name = "${var.name}_batch_${data.aws_region.current.name}"
  }
}



resource "aws_route" "r" {
  route_table_id            = aws_vpc.batch_vpc.main_route_table_id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                =  aws_internet_gateway.igw.id
  depends_on                = [aws_vpc.batch_vpc]
}



resource "aws_subnet" "batch_vpc_subnet" {
  vpc_id     = aws_vpc.batch_vpc.id
  cidr_block = "10.1.1.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.name}_batch_${data.aws_region.current.name}"
  }
}

resource "aws_subnet" "batch_vpc_subnet_second" {
  vpc_id     = aws_vpc.batch_vpc.id
  cidr_block = "10.1.2.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.name}_batch_${data.aws_region.current.name}"
  }
}

resource "aws_db_subnet_group" "default" {
  name       = "${var.name}_batch_${data.aws_region.current.name}"
  subnet_ids = [aws_subnet.batch_vpc_subnet.id, aws_subnet.batch_vpc_subnet_second.id]

  tags = {
    Name = "${var.name}_batch_${data.aws_region.current.name}"
  }
}

resource "aws_batch_compute_environment" "batch" {
  compute_environment_name = "${var.name}_batch_${data.aws_region.current.name}"

  compute_resources {
    instance_role = aws_iam_instance_profile.ecs_instance_role.arn

    instance_type = [
      "c4.large",
    ]

    max_vcpus = 16
    min_vcpus = 2

    security_group_ids = [
      aws_security_group.sec_group.id,
    ]

    subnets = [
      aws_subnet.batch_vpc_subnet.id,
      aws_subnet.batch_vpc_subnet_second.id,
    ]

    type = "EC2"
  }

  service_role = aws_iam_role.aws_batch_service_role.arn
  type         = "MANAGED"
  depends_on   = [aws_iam_role_policy_attachment.aws_batch_service_role]
}

output "batch_compute_arn" {
  value = "${aws_batch_compute_environment.batch.arn}"
}
