variable "name" {
  type = string
  description = "Name used as prefix for batch environment"
}
