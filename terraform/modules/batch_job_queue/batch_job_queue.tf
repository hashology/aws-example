data "aws_region" "current" {}

resource "aws_batch_job_queue" "queue" {
  name     = "${var.name}_${data.aws_region.current.name}"
  state    = "ENABLED"
  priority = 1
  compute_environments = [
    "${var.batch_compute_arn}"
  ]
}

output "batch_job_queue_arn" {
  value = "${aws_batch_job_queue.queue.arn}"
}
