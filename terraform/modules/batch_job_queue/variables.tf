variable "batch_compute_arn" {
  type = string
  description = "ARN for Batch Compute Environment"
}

variable "name" {
  type = string
  description = "Name of the batch job queue"
}
