resource "aws_ec2_transit_gateway_peering_attachment_accepter" "tgw" {
  transit_gateway_attachment_id = var.transit_gateway_attachment_id
  tags = var.tags
  provisioner "local-exec" {
    command = "../../../../utilities/updateTransitRoutes.sh"
  }
}