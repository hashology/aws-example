variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "transit_gateway_attachment_id" {

}