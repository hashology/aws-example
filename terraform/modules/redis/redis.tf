resource "aws_security_group" "redis_sg" {
  name        = "${var.tags["Name"]}-sg"
  description = "${var.tags["Name"]}-sg"
  vpc_id      = var.vpc_id

  tags = var.tags
}

/* can't use inline rules and aws_security_group_role at same sg, hence annoyingly defining them all here externally */
resource "aws_security_group_rule" "redis_ingress" {
  type                     = "ingress"
  from_port                = 6379
  to_port                  = 6379
  protocol                 = "tcp"
  cidr_blocks       = ["10.0.0.0/8"]
  security_group_id = aws_security_group.redis_sg.id
}

resource "aws_security_group_rule" "redis_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.redis_sg.id
}

resource "aws_elasticache_subnet_group" "cache_subnet_group" {
  name       = "${var.tags["Name"]}-subgroup"
  subnet_ids = var.subnet_ids
}



resource "aws_elasticache_replication_group" "cluster" {
  automatic_failover_enabled    = false
  replication_group_id          = var.tags["Name"]
  replication_group_description = "Redis node for ${var.tags["Name"]}"
  node_type                     = var.redis_node_type

  number_cache_clusters         = var.redis_num_cache_nodes
  subnet_group_name             = aws_elasticache_subnet_group.cache_subnet_group.name
  security_group_ids            = [aws_security_group.redis_sg.id]
  engine_version                = var.redis_version
  at_rest_encryption_enabled    = true
  port                          = 6379
  apply_immediately             = true
  
  tags = var.tags
}


output "sg_id" {
  value = aws_security_group.redis_sg.id
}

output "redis_address" {
  value = aws_elasticache_replication_group.cluster.primary_endpoint_address
}
