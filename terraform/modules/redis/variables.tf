variable "subnet_ids" {
  type        = list(string)
  description = "List of subnet ids to place the environment's resources in. Set this to the base module's output variable `subnet_ids`"
}

variable "tags" {
  description = "Map of tags to attach to AWS resource. hashology_client and hashology_environment required"
  type        = map(any)
}

variable "redis_node_type" {
  description = "Instance type of the elasticache redis node"
}

variable "redis_num_cache_nodes" {
  description = "Number of redis nodes in the elasticache cluster"
}

variable "vpc_id" {
  description = "The VPC id from the network module"
}

variable "redis_version" {
  description = "Version of Redis"
}
