variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "custom_routes" {
  description = "Custom routes to add to the VPC"
  type        = map(any)
  default = {}
}

variable "base_cidr_block" {
  description = "CIDR block for VPC"
}

variable "nat_gateway" {
  description = "Boolean for NAT Gateway creation"
  default = false
}