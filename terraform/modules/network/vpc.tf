data "aws_availability_zones" "azs" {}

resource "aws_vpc" "vpc" {
  cidr_block           = var.base_cidr_block
  assign_generated_ipv6_cidr_block = true
  enable_dns_support   = true
  enable_dns_hostnames = true

  
  tags = var.tags
}


resource "aws_subnet" "eks_subnets" {
  vpc_id = aws_vpc.vpc.id

  #count                   = "${length(data.aws_availability_zones.azs.names)}"
  count                   = "2"
  cidr_block              = cidrsubnet(var.base_cidr_block, 4, count.index)
  ipv6_cidr_block         = cidrsubnet(aws_vpc.vpc.ipv6_cidr_block, 8, count.index)
  availability_zone       = element(data.aws_availability_zones.azs.names, count.index)
  map_public_ip_on_launch = true

  tags = merge(
    var.tags,
    {
      Name = "${var.tags["Name"]}-subnet-${data.aws_availability_zones.azs.names[count.index]}"
      "kubernetes.io/role/internal-elb" = 1
      "kubernetes.io/role/elb" = 1
      "kubernetes.io/cluster/${var.tags["Name"]}" = "shared"
    }
  )
}

resource "aws_subnet" "db_subnets" {
  vpc_id = aws_vpc.vpc.id
  count                   = "2"
  cidr_block              = cidrsubnet(var.base_cidr_block, 4, tonumber(count.index) + 4)
  availability_zone       = element(data.aws_availability_zones.azs.names, count.index)
  map_public_ip_on_launch = false

  tags = merge(
    var.tags,
    {
      Name = "${var.tags["Name"]}-subnet-${data.aws_availability_zones.azs.names[count.index]}-db"

    }
  )
}

resource "aws_eip" "nat_gateway" {
  count    = var.nat_gateway ? 1 : 0
  vpc      = true
  tags = merge(
    var.tags,
    {
      Name = "${var.tags["Name"]}-nat-gw"
    }
  )
}

resource "aws_nat_gateway" "gw" {
  count    = var.nat_gateway ? 1 : 0
  allocation_id = aws_eip.nat_gateway[0].id
  subnet_id     = aws_subnet.eks_subnets[2].id

  tags = merge(
    var.tags,
    {
      Name = "${var.tags["Name"]}-nat-gw"
    }
  )
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.vpc.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  lifecycle {
    ignore_changes = [
      route,
    ]
  }
  
  tags = merge(
    var.tags,
    {
      Name = "${var.tags["Name"]}-vpc"
    }
  )
}

resource "aws_route" "custom_routes" {
  for_each = var.custom_routes
  route_table_id            = aws_route_table.route_table.id

  destination_cidr_block = lookup(each.value, "destination_cidr_block", null)
  transit_gateway_id = lookup(each.value, "transit_gateway_id", null)
  depends_on = [aws_route_table.route_table]
}

resource "aws_route_table_association" "public" {
  count          = length(data.aws_availability_zones.azs.names)
  subnet_id      = element(aws_subnet.eks_subnets.*.id, count.index)
  route_table_id = aws_route_table.route_table.id
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    var.tags,
    {
      Name = "${var.tags["Name"]}-igw"
    }
  )
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "subnet_ids" {
  value = aws_subnet.eks_subnets[*].id
}
output "eks_subnet_ids" {
  value = aws_subnet.eks_subnets[*].id
}

output "db_subnet_ids" {
  value = aws_subnet.db_subnets[*].id
}

output "route_table_id" {
  value = aws_vpc.vpc.main_route_table_id
}