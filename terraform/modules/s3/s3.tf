resource "aws_iam_user" "user" {
  name = "${var.tags["application"]}-${var.tags["environment"]}"

  tags = {
    Name              = "${var.tags["application"]}-${var.tags["environment"]}"
    client            = var.tags["client"]
    environment       = var.tags["environment"]
    application       = var.tags["application"]
    owner             = var.tags["owner"]
  }
}

resource "aws_iam_policy" "user_policy" {
  name        = "${var.tags["application"]}-${var.tags["environment"]}-user"
  path        = "/"
  description = "Policy for ${var.tags["application"]}-${var.tags["environment"]} user"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "s3:ListBucket"
        ],
        "Resource": "arn:aws:s3:::${aws_s3_bucket.main_bucket.id}"
    },
    {
        "Effect": "Allow",
        "Action": [
            "s3:GetObject",
            "s3:GetObjectAcl",
            "s3:PutObject",
            "s3:PutObjectAcl"
        ],
        "Resource": "arn:aws:s3:::${aws_s3_bucket.main_bucket.id}/*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "user_policy" {
  name       = "${var.tags["application"]}-${var.tags["environment"]}"
  users      = ["${aws_iam_user.user.name}"]
  policy_arn = aws_iam_policy.user_policy.arn
}

resource "aws_s3_bucket" "main_bucket" {
  bucket = "${var.tags["application"]}-${var.tags["environment"]}"
  acl    = "private"

  tags = {
    client            = var.tags["client"]
    environment       = var.tags["environment"]
    application       = var.tags["application"]
    owner             = var.tags["owner"]
  }
}

resource "aws_iam_access_key" "user" {
  user    = aws_iam_user.user.name
}

output "main_bucket" {
  value = "${aws_s3_bucket.main_bucket.id}"
}

output "bucket_aws_id" {
  value = "${aws_iam_access_key.user.id}"
}
output "bucket_aws_secret" {
  value = "${aws_iam_access_key.user.secret}"
}
