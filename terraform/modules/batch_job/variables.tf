variable "name" {
  type = string
  description = "Name of the job"
}

variable "script" {
  type = string
  description = "Path to script"
}

variable "schedule" {
  type = string
  description = "Cron schedule for the script"
}

variable "batch_job_queue_arn" {
  type = string
  description = "ARN for Batch Job Queue"
}
