data "aws_region" "current" {}


resource "aws_batch_job_definition" "job_def" {
  name     = "${var.name}_${data.aws_region.current.name}"
  type = "container"

  container_properties = <<CONTAINER_PROPERTIES
{
    "image": "499602364333.dkr.ecr.us-east-1.amazonaws.com/devops/fetch-and-run:latest",
    "memory": 2048,
    "vcpus": 2,
    "volumes": [
      {
        "host": {
          "sourcePath": "/tmp"
        },
        "name": "tmp"
      }
    ],
    "environment": [
        {"name": "BATCH_FILE_TYPE", "value": "script"},
        {"name": "BATCH_FILE_S3_URL", "value": "s3://hashology-devops/${var.script}"}
    ],
    "mountPoints": [
        {
          "sourceVolume": "tmp",
          "containerPath": "/tmp",
          "readOnly": false
        }
    ],
    "ulimits": [
      {
        "hardLimit": 1024,
        "name": "nofile",
        "softLimit": 1024
      }
    ]
}
CONTAINER_PROPERTIES
}

resource "aws_cloudwatch_event_rule" "rule" {
  name        = "${var.name}_${data.aws_region.current.name}_trigger"
  schedule_expression    = "cron(${var.schedule})"
}

resource "aws_cloudwatch_event_target" "target" {
  rule      = aws_cloudwatch_event_rule.rule.name
  arn       = var.batch_job_queue_arn
  role_arn  = "arn:aws:iam::499602364333:role/TriggerBatch"
  batch_target {
    job_definition = aws_batch_job_definition.job_def.arn
    job_name = "${var.name}_${data.aws_region.current.name}_job"
  }

}
