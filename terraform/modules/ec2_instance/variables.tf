variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "type" {
  default="t3.micro"
}

variable "subnet_id" {
  
}

variable "user_data" {
  default=""
}
variable "disk_size" {
  default = 10
}

variable "public_ip" {
  default = true
}

variable "vpc_sec_group" {

}

variable "dns_name" {
  default = ""
}

