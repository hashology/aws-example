data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_route53_zone" "selected" {
  count   = var.dns_name != "" ? 1 : 0
  name         = "${element(split(".", var.dns_name), length(split(".", var.dns_name)) -2)}.${element(split(".", var.dns_name), length(split(".", var.dns_name)) -1)}"
}

resource "aws_instance" "instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.type
  user_data = var.user_data
  key_name = "devops"
  root_block_device  {
    volume_size = var.disk_size
  }
  vpc_security_group_ids = [var.vpc_sec_group]
  subnet_id = var.subnet_id
  associate_public_ip_address = var.public_ip ? true : false
  tags = var.tags
  lifecycle {
    ignore_changes = [ami, user_data]
  }
}

resource "aws_route53_record" "ec2" {
  count   = var.dns_name != "" ? 1 : 0
  zone_id = data.aws_route53_zone.selected[0].zone_id
  name    = "${var.dns_name}."
  type    = "CNAME"
  ttl     = "60"
  records = [aws_instance.instance.public_dns]
}