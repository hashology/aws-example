variable "tags" {
  description = "Map of tags to attach to AWS resource."
  type        = map(string)
}

variable "subnet_ids" {

}
variable "vpc_id" {

}
variable "transit_gateway_id" {

}