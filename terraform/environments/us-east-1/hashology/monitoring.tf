module "elasticsearch" {
  source = "../../../modules/elasticsearch"

  vpc_id = module.network.vpc_id
  node_count = 2
  subnet_ids = module.network.subnet_ids
  instance_class = "m5.large.elasticsearch"
  master_instance_class = "t3.medium.elasticsearch"
  volume_size_gb = 100
  es_version = 7.9

  es_dns_name = "elasticsearch.hashology.io"

  tags = merge(
    module.configuration.tags,
    {
      Name = "monitoring"
    }
  ) 
}

