module "vpn_sec_group" {
  source = "../../../modules/security_group"
  vpc_id = module.network.vpc_id

  ingress = [{
    from_port   = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port   = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }, {
    from_port   = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }]

  tags = merge(
    module.configuration.tags,
    {
      Name = "VPN Server Security Group"
    }
  ) 
}

module "ec2_instance" {
  source = "../../../modules/ec2_instance"
  type = "t3.micro"
  disk_size = 10
  public_ip = true
  vpc_sec_group = module.vpn_sec_group.id
  subnet_id = module.network.subnet_ids[0]
  dns_name = "vortex.hashology.io"

  user_data = <<EOF
#! /bin/bash

set -x
exec > >(tee /var/log/user-data.log|logger -t user-data ) 2>&1
echo BEGIN

##Install OpenVPN AS
apt update && apt -y install ca-certificates wget net-tools gnupg
wget -qO - https://as-repository.openvpn.net/as-repo-public.gpg | apt-key add -
echo "deb http://as-repository.openvpn.net/as/debian focal main">/etc/apt/sources.list.d/openvpn-as-repo.list
apt update && apt -y install openvpn-as
echo -e "openvpn\nopenvpn" | passwd openvpn

##Setup LetsEncrypt

apt-get -y install software-properties-common
add-apt-repository -y ppa:certbot/certbot
apt-get -y update
apt-get -y install certbot

sleep 60 #wait for dns propogations

sudo certbot certonly \
--standalone \
--non-interactive \
--agree-tos \
--email matthias@hashology.io \
--domains vortex.hashology.io \
--pre-hook 'sudo service openvpnas stop' \
--post-hook 'sudo service openvpnas start'

sudo ln -s -f /etc/letsencrypt/live/vortex.hashology.io/cert.pem /usr/local/openvpn_as/etc/web-ssl/server.crt
sudo ln -s -f /etc/letsencrypt/live/vortex.hashology.io/privkey.pem /usr/local/openvpn_as/etc/web-ssl/server.key
sudo service openvpnas stop
sudo service openvpnas start
sleep 15 #wait for service to start

sudo /usr/local/openvpn_as/scripts/sacli --key cs.https.port --value 443 ConfigPut
sudo /usr/local/openvpn_as/scripts/sacli --key "vpn.server.routing.private_network.0" --value "10.0.0.0/8" ConfigPut
sudo /usr/local/openvpn_as/scripts/sacli --key "host.name" --value "vortex.hashology.io" ConfigPut
sudo /usr/local/openvpn_as/scripts/sacli start

cat <<EOT >> /etc/cron.monthly/renewCert.sh
#!/bin/bash
certbot renew --pre-hook "service openvpnas stop" --post-hook "service openvpnas start"
EOT

systemctl enable cron
systemctl start cron

EOF

  tags = merge(
    module.configuration.tags,
    {
      Name = "VPN Server"
    }
  )
}