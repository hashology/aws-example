module "configuration" {
  source = "../../../modules/configuration"

  tags = {
    Name        = "internal-operations"
    prod        = "true"
    owner       = "DevOps"
  }
}

module "network" {
  source = "../../../modules/network"

  base_cidr_block = "10.1.0.0/16"

  tags = module.configuration.tags
}


module "transit_gateway" {
  source = "../../../modules/transit_gateway"
  
  asn = 65533

  tags = merge(
    module.configuration.tags,
    {
      Name = "core-tgw"
    }
  )
}

module "tgw_attachment" {
  source = "../../../modules/transit_gateway_attachment"
  
  subnet_ids = module.network.subnet_ids
  vpc_id = module.network.vpc_id
  transit_gateway_id = module.transit_gateway.id

  tags = merge(
    module.configuration.tags,
    {
      Name = "Core_and_InternalOperations"
    }
  )
}

