terraform {
  backend "s3" {
    bucket = "hashology-internal"
    region = "us-east-1"
  }
}
