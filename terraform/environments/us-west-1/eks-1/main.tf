#US-East-Core TGW
data "aws_ec2_transit_gateway" "core_us_east_1" {
  provider = aws.us_east_1

  filter {
    name   = "tag:Name"
    values = ["core-tgw"]
  }
  filter {
    name   = "state"
    values = ["available"]
  }
}

data "aws_region" "current" {}

module "configuration" {
  source = "../../../modules/configuration"

  tags = {
    Name        = "hashology-${data.aws_region.current.name}-eks-1"
    prod        = "true"
    owner       = "DevOps"
  }
}

module "network" {
  source = "../../../modules/network"

  base_cidr_block = "10.2.0.0/16"

  tags = merge(
    module.configuration.tags,
    {
      Name = "hashology-${data.aws_region.current.name}-eks-1"
    }
  )
}


module "eks" {
  source = "../../../modules/eks"

  cluster_version = "1.17"
  subnet_ids    = module.network.eks_subnet_ids

  public_api_whitelist = [
    "99.30.124.235/32",
  ]

  node_groups = [
    {
      name = "default"
      max_node_count    = 20
      ami_type = "AL2_ARM_64"
      instance_type = "m6g.xlarge"
      disk_size = 100
      labels = {
        capacityType = "ON_DEMAND"
      }
    },
    {
      name = "administrative"
      max_node_count    = 3
      ami_type = "AL2_x86_64"
      instance_type = "t3.small"
      disk_size = 10
      labels = {
        capacityType = "ON_DEMAND"
      }
    },
  ]

  spot_node_groups = [
    {
      name = "spot_default"
      max_node_count    = 20
      ami_type = "AL2_ARM_64"
      instance_types = ["m6g.large", "r6g.medium", "m6g.xlarge", "r6g.large"]
      disk_size = 100
      labels = {
        capacityType = "SPOT"
      }
    }
  ]

  s3_access_buckets = [
    "hashology.staging"
  ]

  tags = module.configuration.tags
}

module "transit_gateway" {
  source = "../../../modules/transit_gateway"
  
  asn = 65532

  tags = merge(
    module.configuration.tags,
    {
      Name = "core-tgw"
    }
  )
}

module "tgw_attachment" {
  source = "../../../modules/transit_gateway_attachment"
  
  subnet_ids = module.network.subnet_ids
  vpc_id = module.network.vpc_id
  transit_gateway_id = module.transit_gateway.id

  
  tags = merge(
    module.configuration.tags,
    {
      Name = "Core and ${data.aws_region.current.name}-eks-1"
    }
  )
}

module "tgw_peer_main" {
  source = "../../../modules/transit_gateway_peer"

  transit_gateway_id = module.transit_gateway.id
  peer_transit_gateway_id = data.aws_ec2_transit_gateway.core_us_east_1.id
  tags = merge(
    module.configuration.tags,
    {
      Name = "TGW Core us-east-1"
    }
  )
}

module "tgw_peer_accepter" {
  source = "../../../modules/transit_gateway_accepter"
  providers = {
    aws = aws.us_east_1
  }
  transit_gateway_attachment_id = module.tgw_peer_main.attachment_id
  tags = merge(
    module.configuration.tags,
    {
      Name = "TGW Core ${data.aws_region.current.name}"
    }
  )
}
