resource "aws_s3_bucket_object" "object" {
  bucket = "hashology-devops"
  key    = basename("../../../scripts/backup_rds_to_s3.sh")
  source = "../../../scripts/backup_rds_to_s3.sh"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("../../../scripts/backup_rds_to_s3.sh")
}

resource "aws_s3_bucket_object" "backup_s3_script" {
  bucket = "hashology-devops"
  key    = basename("../../../scripts/backup_s3.sh")
  source = "../../../scripts/backup_s3.sh"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("../../../scripts/backup_s3.sh")
}


module "batch_compute" {
  source = "../../../modules/batch_compute"
  name = "backups"
}

module "batch_job_queue" {
  source = "../../../modules/batch_job_queue"

  name = "backups"
  batch_compute_arn = "${module.batch_compute.batch_compute_arn}"
}

module "batch_job" {
  source = "../../../modules/batch_job"

  script = "backup_rds_to_s3.sh"
  name = "rds-backups"
  schedule = "0 0 * * ? *"
  batch_job_queue_arn = "${module.batch_job_queue.batch_job_queue_arn}"
}

module "s3_batch_job_queue" {
  source = "../../../modules/batch_job_queue"

  name = "s3_backups"
  batch_compute_arn = module.batch_compute.batch_compute_arn
}

module "s3_batch_job" {
  source = "../../../modules/batch_job"

  script = "backup_s3.sh"
  name = "s3_backups"
  schedule = "0 0 * * ? *"
  batch_job_queue_arn = module.s3_batch_job_queue.batch_job_queue_arn
}