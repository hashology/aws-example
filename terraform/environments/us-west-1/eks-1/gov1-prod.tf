
module "gov1_prod_redis" {
  source = "../../../modules/redis"

  redis_version = "5.0.6"
  redis_node_type = "cache.m6g.large"
  redis_num_cache_nodes = 1

  vpc_id = module.network.vpc_id
  subnet_ids = module.network.db_subnet_ids

  tags = merge(
  module.configuration.tags,
  {
    Name = "gov1-prod-redis"
  }
  )
}

module "gov1_prod_rds" {
  source = "../../../modules/rds"

  db_allocated_storage = 1024
  db_instance_class    = "db.t3.large"
  db_backup_window     = "02:00-03:00"

  vpc_id      = module.network.vpc_id
  eks_subnet_ids = module.network.eks_subnet_ids
  db_subnet_ids  = module.network.db_subnet_ids

  db_parameter_group = "hashology56-binlog-row"
  mysql_version = "5.6.48"
  storage_type = "gp2"
  public_access = false
  multi_az = false

  tags = merge(
  module.configuration.tags,
  {
    Name = "gov1-prod-rds"
  }
  )
}

module "gov1_prod_elasticsearch" {
  source = "../../../modules/elasticsearch"

  node_count = 2
  master_instance_class = "t3.small.elasticsearch"
  instance_class = "r4.xlarge.elasticsearch"
  es_version = "7.1"
  volume_size_gb = 150

  vpc_id      = module.network.vpc_id
  subnet_ids  = module.network.db_subnet_ids

  tags = merge(
  module.configuration.tags,
  {
    Name = "gov1-prod-elasticsearch"
  }
  )
}

module "gov1_prod_secrets" {
  source = "../../../modules/secrets"

  providers = {
    aws = aws.us_east_1
  }
  
  base_secret_name = "gov1/prod/secret-base"

  tags = merge(
    module.configuration.tags,
    {
      Name = "gov1/prod/hashology"
    }
  )

  secrets = {
    DB_HOST = module.gov1_prod_rds.db_address
    DB_ROOT_USERNAME = "hashology"
    DB_ROOT_PASSWORD = module.gov1_prod_rds.db_password
    REDIS_HOST = module.gov1_prod_redis.redis_address
    ES_HOST = module.gov1_prod_elasticsearch.es_endpoint
    ES_URL = "https://${module.gov1_prod_elasticsearch.es_endpoint}:443"
    REDIS_URL = "redis://${module.gov1_prod_redis.redis_address}:6379"
  }

}

output "service-account-arn" {
  value = module.eks.service-account-arn
}

output "gov1_prod_db_address" {
  value = module.gov1_prod_rds.db_address
}

output "gov1_prod_db_password" {
  value = module.gov1_prod_rds.db_password
}

output "gov1_prod_redis_address" {
  value = module.gov1_prod_redis.redis_address
}

output "gov1_prod_es_endpoint" {
  value = module.gov1_prod_elasticsearch.es_endpoint
}
