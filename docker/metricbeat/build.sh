#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "Usage: $0 <metricbeat version>"
  exit 1
fi

VERSION=$1
ECR_REPO=999999999999.dkr.ecr.us-east-1.amazonaws.com
aws ecr get-login-password \
    --region us-east-1 \
| docker login \
    --username AWS \
    --password-stdin ${ECR_REPO}


docker buildx build --push --progress auto --platform linux/arm64,linux/amd64 -t ${ECR_REPO}/metricbeat:${VERSION} --build-arg VERSION=${VERSION} .
