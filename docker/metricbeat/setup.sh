#!/bin/bash

if [ "$TARGETPLATFORM" == "linux/amd64" ]; then
  ARCH="x86_64";
elif [ "$TARGETPLATFORM" == "linux/arm64" ]; then
  ARCH="arm64";
fi
wget -O- https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-oss-$VERSION-linux-$ARCH.tar.gz | tar xz
mv metricbeat-$VERSION-linux-${ARCH}/* . && rm -rf metricbeat-$VERSION-linux-$ARCH